package networking6;
import java.net.*;
import java.io.*;

public class Chat {
	
	public static void main(String[] args){
		// TODO Auto-generated method stub
		
		//Reader for the UserInput - read from console
        BufferedReader userin = new BufferedReader(new InputStreamReader(System.in));
        
        String hostName = "192.168.0.100"; 					//only needed if you try to connect
        int portNumber = 1555;								//standard port for serversocket
        
        ServerSocket serverSocket = null;					//declaration of serverSocket - needed if you are the host
        Socket socket = null;								//declaration of socket - for in- and output!
        String choice=null;
        System.out.println("Want be the host? (y/n)");

		try {
			choice = userin.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (choice.equals("y")){							//if you are the host
			try {
				serverSocket = new ServerSocket(portNumber);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	//create a serverSocket and wait for incoming connection
			System.out.println("Waiting for your mate!");
			try {
				socket = serverSocket.accept();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					//if the acceptmethod gets a client it returns a socket
			System.out.println("Here he/she is.");
		}
		if (choice.equals("n")){							//if you are the client
			System.out.println("Which IP?");
	        try {
				hostName = userin.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}					//save the IP to the field hostname
	        System.out.println("Which port?");
	        try {
				portNumber = Integer.parseInt(userin.readLine());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //save the port of the serversocket from other pc 
			try {
				socket = new Socket(hostName, portNumber);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	 	//create a socket with the arguments above. connection established!
			System.out.println("You are connected!");
		}		
		       
        //Writer for the output
        PrintWriter outgoing = null;
		try {
			outgoing = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        //Reader for the Input
        BufferedReader incoming = null;
		try {
			incoming = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	        
        //Added 2threads for the two methods
        Thread t1 = new Thread(new In(incoming));
        t1.start();
        Thread t2 = new Thread(new Out(outgoing, userin));
        t2.start();
        }		
}