package networking6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

//implements the Runnable Interface. Needed for creating threads. 
//The class has to implement the run method!
public class Out implements Runnable {
	
	//fields
	private PrintWriter outgoing;  
	private BufferedReader userin;

	//constructor
	public Out(PrintWriter outgoing,  BufferedReader userin) {
		this.outgoing=outgoing;
		this.userin=userin;
		// TODO Auto-generated constructor stub
	}

	//Overide the run method (or implement it)
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String userInput;
        try {
			while ((userInput = userin.readLine()) != null) {	//if there is userinput
				outgoing.println(userInput);					//send this to the output
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
