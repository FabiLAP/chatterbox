package networking6;

import java.io.BufferedReader;
import java.io.IOException;

//implements the Runnable Interface. Needed for creating threads. 
//The class has to implement the run method!
public class In implements Runnable {

	//field
	private BufferedReader in;
	
	//constructor, needs a Reader
	public In(BufferedReader in) {
		this.in=in;
	}	
	
	//Overide the run method (or implement it)
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String inputLine;
		try {
			while ((inputLine = in.readLine()) != null) {				//If there is input
				System.out.println("incoming message: "+inputLine);		//print out the input
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
